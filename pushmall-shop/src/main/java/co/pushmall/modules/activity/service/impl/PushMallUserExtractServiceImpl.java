package co.pushmall.modules.activity.service.impl;

import co.pushmall.modules.activity.domain.PushMallUserExtract;
import co.pushmall.modules.activity.repository.PushMallUserExtractRepository;
import co.pushmall.modules.activity.service.PushMallUserExtractService;
import co.pushmall.modules.activity.service.dto.PushMallUserExtractDTO;
import co.pushmall.modules.activity.service.dto.PushMallUserExtractQueryCriteria;
import co.pushmall.modules.activity.service.mapper.PushMallUserExtractMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-11-14
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallUserExtractServiceImpl implements PushMallUserExtractService {

    private final PushMallUserExtractRepository pushMallUserExtractRepository;

    private final PushMallUserExtractMapper pushMallUserExtractMapper;

    public PushMallUserExtractServiceImpl(PushMallUserExtractRepository pushMallUserExtractRepository, PushMallUserExtractMapper pushMallUserExtractMapper) {
        this.pushMallUserExtractRepository = pushMallUserExtractRepository;
        this.pushMallUserExtractMapper = pushMallUserExtractMapper;
    }

    @Override
    public Map<String, Object> queryAll(PushMallUserExtractQueryCriteria criteria, Pageable pageable) {
        Page<PushMallUserExtract> page = pushMallUserExtractRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(pushMallUserExtractMapper::toDto));
    }

    @Override
    public List<PushMallUserExtractDTO> queryAll(PushMallUserExtractQueryCriteria criteria) {
        return pushMallUserExtractMapper.toDto(pushMallUserExtractRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallUserExtractDTO findById(Integer id) {
        Optional<PushMallUserExtract> yxUserExtract = pushMallUserExtractRepository.findById(id);
        ValidationUtil.isNull(yxUserExtract, "PushMallUserExtract", "id", id);
        return pushMallUserExtractMapper.toDto(yxUserExtract.get());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallUserExtractDTO create(PushMallUserExtract resources) {
        return pushMallUserExtractMapper.toDto(pushMallUserExtractRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallUserExtract resources) {
        Optional<PushMallUserExtract> optionalPushMallUserExtract = pushMallUserExtractRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallUserExtract, "PushMallUserExtract", "id", resources.getId());
        PushMallUserExtract pushMallUserExtract = optionalPushMallUserExtract.get();
        pushMallUserExtract.copy(resources);
        pushMallUserExtractRepository.save(pushMallUserExtract);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallUserExtractRepository.deleteById(id);
    }
}
