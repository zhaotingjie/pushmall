package co.pushmall.modules.activity.rest;

import cn.hutool.core.util.ObjectUtil;
import co.pushmall.aop.log.Log;
import co.pushmall.modules.activity.domain.PushMallStoreCouponIssue;
import co.pushmall.modules.activity.service.PushMallStoreCouponIssueService;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponIssueQueryCriteria;
import co.pushmall.utils.OrderUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pushmall
 * @date 2019-11-09
 */
@Api(tags = "商城:优惠券发布管理")
@RestController
@RequestMapping("api")
public class StoreCouponIssueController {

    private final PushMallStoreCouponIssueService pushMallStoreCouponIssueService;

    public StoreCouponIssueController(PushMallStoreCouponIssueService pushMallStoreCouponIssueService) {
        this.pushMallStoreCouponIssueService = pushMallStoreCouponIssueService;
    }

    @Log("查询已发布")
    @ApiOperation(value = "查询已发布")
    @GetMapping(value = "/PmStoreCouponIssue")
    @PreAuthorize("@el.check('admin','YXSTORECOUPONISSUE_ALL','YXSTORECOUPONISSUE_SELECT')")
    public ResponseEntity getPushMallStoreCouponIssues(PushMallStoreCouponIssueQueryCriteria criteria, Pageable pageable) {
        criteria.setIsDel(0);
        return new ResponseEntity(pushMallStoreCouponIssueService.queryAll(criteria, pageable), HttpStatus.OK);
    }

    @Log("发布")
    @ApiOperation(value = "发布")
    @PostMapping(value = "/PmStoreCouponIssue")
    @PreAuthorize("@el.check('admin','YXSTORECOUPONISSUE_ALL','YXSTORECOUPONISSUE_CREATE')")
    public ResponseEntity create(@Validated @RequestBody PushMallStoreCouponIssue resources) {
        if (ObjectUtil.isNotNull(resources.getStartTimeDate())) {
            resources.setStartTime(OrderUtil.
                    dateToTimestamp(resources.getStartTimeDate()));
        }
        if (ObjectUtil.isNotNull(resources.getEndTimeDate())) {
            resources.setEndTime(OrderUtil.
                    dateToTimestamp(resources.getEndTimeDate()));
        }
        if (resources.getTotalCount() > 0) {
            resources.setRemainCount(resources.getTotalCount());
        }
        resources.setAddTime(OrderUtil.getSecondTimestampTwo());
        return new ResponseEntity(pushMallStoreCouponIssueService.create(resources), HttpStatus.CREATED);
    }

    @Log("修改状态")
    @ApiOperation(value = "修改状态")
    @PutMapping(value = "/PmStoreCouponIssue")
    @PreAuthorize("@el.check('admin','YXSTORECOUPONISSUE_ALL','YXSTORECOUPONISSUE_EDIT')")
    public ResponseEntity update(@Validated @RequestBody PushMallStoreCouponIssue resources) {
        pushMallStoreCouponIssueService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除")
    @ApiOperation(value = "删除")
    @DeleteMapping(value = "/PmStoreCouponIssue/{id}")
    @PreAuthorize("@el.check('admin','YXSTORECOUPONISSUE_ALL','YXSTORECOUPONISSUE_DELETE')")
    public ResponseEntity delete(@PathVariable Integer id) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        PushMallStoreCouponIssue resources = new PushMallStoreCouponIssue();
        resources.setId(id);
        resources.setIsDel(1);
        pushMallStoreCouponIssueService.update(resources);
        return new ResponseEntity(HttpStatus.OK);
    }
}
