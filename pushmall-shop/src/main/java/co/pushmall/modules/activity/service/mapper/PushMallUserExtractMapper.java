package co.pushmall.modules.activity.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.modules.activity.domain.PushMallUserExtract;
import co.pushmall.modules.activity.service.dto.PushMallUserExtractDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-11-14
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallUserExtractMapper extends EntityMapper<PushMallUserExtractDTO, PushMallUserExtract> {

}
