package co.pushmall.modules.activity.service;

import co.pushmall.modules.activity.domain.PushMallStoreCouponIssueUser;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponIssueUserDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponIssueUserQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-11-09
 */
//@CacheConfig(cacheNames = "yxStoreCouponIssueUser")
public interface PushMallStoreCouponIssueUserService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallStoreCouponIssueUserQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallStoreCouponIssueUserDTO> queryAll(PushMallStoreCouponIssueUserQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallStoreCouponIssueUserDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallStoreCouponIssueUserDTO create(PushMallStoreCouponIssueUser resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallStoreCouponIssueUser resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);
}
