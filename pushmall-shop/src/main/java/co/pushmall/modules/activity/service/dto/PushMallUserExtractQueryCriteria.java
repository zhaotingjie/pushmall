package co.pushmall.modules.activity.service.dto;

import co.pushmall.annotation.Query;
import lombok.Data;

/**
 * @author pushmall
 * @date 2019-11-14
 */
@Data
public class PushMallUserExtractQueryCriteria {

    // 模糊
    @Query(type = Query.Type.INNER_LIKE)
    private String realName;
}
