package co.pushmall.modules.activity.repository;

import co.pushmall.modules.activity.domain.PushMallStoreCombination;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * @author pushmall
 * @date 2019-11-18
 */
public interface PushMallStoreCombinationRepository extends JpaRepository<PushMallStoreCombination, Integer>, JpaSpecificationExecutor {
    @Modifying
    @Query(value = "update pushmall_store_combination set is_show = ?1 where id = ?2", nativeQuery = true)
    void updateOnsale(int status, int id);

}
