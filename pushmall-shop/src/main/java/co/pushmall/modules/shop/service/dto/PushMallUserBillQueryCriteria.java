package co.pushmall.modules.shop.service.dto;

import lombok.Data;

/**
 * @author pushmall
 * @date 2019-11-06
 */
@Data
public class PushMallUserBillQueryCriteria {
    private String nickname;
    private String category;
    private String type;
}
