package co.pushmall.modules.shop.domain;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author pushmall
 * @date 2019-11-02
 */
@Entity
@Data
@Table(name = "pushmall_store_order_status")
public class PushMallStoreOrderStatus implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    // 订单id
    @Column(name = "oid", nullable = false)
    private Integer oid;

    // 操作类型
    @Column(name = "change_type", nullable = false)
    private String changeType;

    // 操作备注
    @Column(name = "change_message", nullable = false)
    private String changeMessage;

    // 操作时间
    @Column(name = "change_time", nullable = false)
    private Integer changeTime;

    public void copy(PushMallStoreOrderStatus source) {
        BeanUtil.copyProperties(source, this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
