package co.pushmall.modules.shop.service;

import co.pushmall.modules.shop.domain.PushMallSystemStoreStaff;
import co.pushmall.modules.shop.service.dto.PushMallSystemStoreStaffDto;
import co.pushmall.modules.shop.service.dto.PushMallSystemStoreStaffQueryCriteria;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2020-03-22
 */
public interface PushMallSystemStoreStaffService {

    /**
     * 查询数据分页
     *
     * @param criteria 条件
     * @param pageable 分页参数
     * @return Map<String, Object>
     */
    Map<String, Object> queryAll(PushMallSystemStoreStaffQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria 条件参数
     * @return List<PushMallSystemStoreStaffDto>
     */
    List<PushMallSystemStoreStaffDto> queryAll(PushMallSystemStoreStaffQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id ID
     * @return PushMallSystemStoreStaffDto
     */
    PushMallSystemStoreStaffDto findById(Integer id);

    /**
     * 创建
     *
     * @param resources /
     * @return PushMallSystemStoreStaffDto
     */
    PushMallSystemStoreStaffDto create(PushMallSystemStoreStaff resources);

    /**
     * 编辑
     *
     * @param resources /
     */
    void update(PushMallSystemStoreStaff resources);

    /**
     * 多选删除
     *
     * @param ids /
     */
    void deleteAll(Integer[] ids);

    /**
     * 导出数据
     *
     * @param all      待导出的数据
     * @param response /
     * @throws IOException /
     */
    void download(List<PushMallSystemStoreStaffDto> all, HttpServletResponse response) throws IOException;

    List<PushMallSystemStoreStaffDto> findByUid(Integer uid, Integer spreadUid);
}
