package co.pushmall.modules.shop.service;

import co.pushmall.modules.shop.domain.PushMallUserBill;
import co.pushmall.modules.shop.service.dto.PushMallUserBillDTO;
import co.pushmall.modules.shop.service.dto.PushMallUserBillQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-11-06
 */
//@CacheConfig(cacheNames = "yxUserBill")
public interface PushMallUserBillService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallUserBillQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallUserBillDTO> queryAll(PushMallUserBillQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallUserBillDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallUserBillDTO create(PushMallUserBill resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallUserBill resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);
}
