package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallExpress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-12-12
 */
public interface PushMallExpressRepository extends JpaRepository<PushMallExpress, Integer>, JpaSpecificationExecutor {

    /**
     * findByCode
     *
     * @param code
     * @return
     */
    PushMallExpress findByCode(String code);
}
