package co.pushmall.mp.repository;


import co.pushmall.mp.domain.PushMallWechatMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-10-06
 */
public interface PushMallWechatMenuRepository extends JpaRepository<PushMallWechatMenu, String>, JpaSpecificationExecutor {
}
